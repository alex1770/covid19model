
import csv,sys,getdata
if sys.version_info[0]<3: print("Requires Python 3");sys.exit(1)

import scipy
import scipy.special
def fact(x): return scipy.special.gamma(x+1)
# If n is negative then r must be an integer in this version (which is true here)
def bin(n,r):
  if n>=0:
    return fact(n)/(fact(r)*fact(n-r))
  else:
    return (-1)**r*bin(r-n-1,r)

from math import log,exp

source="worldometer"
#source="ecdc"

if len(sys.argv)>1: country=sys.argv[1]
else: country="UK"

print("Country:",country)

(dates, confirmed, deaths, recovered, active, newc, newd)=getdata.getcountrydata(country,thr=10,source=source)

#print(dates)
#print(newc)
#print(recovered)
#print(newd)

# UK timeline
# 2020-03-12 People told to isolate if they have certain symptoms [check this date]
# 2020-03-15 Everyone urged to socially distance, work from home if possible, avoid gathering etc.
# 2020-03-20 PM orders pubs, cafes, restaurants, bars, gyms to close; schools closed to most pupils
# 2020-03-23 1.5m especially vulnerable people written to, telling them not to go out
#            Lockdown announced with significant restrictions (technically comes into force 2020-03-26).
specialdates=["2020-03-12","2020-03-15","2020-03-20","2020-03-23"]

# Minimum, maximum time from being infected to being infectious:
mini=2
maxi=14

# subdivisions of a day
# subd=10# not used yet

# l[len(l)-1-i] = simulated number of people who became (newly) infected exactly i days ago

# Crudely estimate early growth rate in order to initialise l[]
gr=(active[10]/active[0])**(1/10)
l0=[(gr-1)/(gr**maxi-1)*active[0]]
for i in range(maxi-1): l0.append(l0[-1]*gr)
l=list(l0)

def getNB(mu,var,mi,mx):
  p=1-mu/var
  assert p>0 and p<1
  r=mu*(1-p)/p
  infect=[0]*(mx+1)
  for k in range(mi,mx+1): infect[k]=bin(k+r-1,k)*p**k
  s=sum(infect)
  for k in range(mi,mx+1): infect[k]/=s
  return infect

# Make something up pro tem
R0=3
infectmean=4
infectvar=7
infect=getNB(infectmean,infectvar,mini,maxi)
RR=[R0, 2.2, 1.8, 1.5, 0.9]

numdays=len(dates)
phase=0

for i in range(numdays):
  date=dates[i]
  while phase<len(specialdates) and date>=specialdates[phase]: phase+=1
  R=RR[phase]
  t=0
  for j in range(mini,maxi+1):
    if j>len(l): break
    t+=infect[j]*l[len(l)-j]
  l.append(R*t)
  print(date,"%9.1f  %6d"%(l[-1],newd[i]))

