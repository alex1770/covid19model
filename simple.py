
import csv,sys,getdata
if sys.version_info[0]<3: print("Requires Python 3");sys.exit(1)

region=""
if len(sys.argv)>1: country=sys.argv[1]
else: country="Italy"

print("# Country:",country)

serialint="""
         -11          1
         -10          2
          -9          1
          -8          1
          -6          2
          -5          5
          -4          8
          -3          7
          -2          7
          -1         25
           0         48
           1         41
           2         50
           3         36
           4         50
           5         30
           6         33
           7         23
           8         20
           9         16
          10         15
          11         14
          12          4
          13          5
          14          4
          15          6
          16          2
          17          2
          20          1
""".strip().split('\n')
serialint=[[int(x) for x in row.split()] for row in serialint]
tot=sum(c for (i,c) in serialint)
serialint=[[i,c/tot] for (i,c) in serialint]

output={}
for smooth in [0,1]:
  (dates, confirmed, deaths, recovered, active, newc, newd)=getdata.getcountrydata(country,smoothlevel=smooth,thr=10,region=region)
  numdays=len(dates)
  for i in range(5,numdays):
    out=""
    for meas in [newc,newd]:
      ta=0
      for (j,p) in serialint:
        ta+=p*meas[min(max(i-j,0),len(meas)-1)]
      if ta>=10: out+="    %6d %6.3f"%(meas[i],meas[i]/ta)
      else: out+="                 "
    if out!=len(out)*" ": output.setdefault(dates[i],{})[smooth]=out

print("Date           ============ RAW ============     ========= SMOOTHED ==========  ")
print("Date           Cases    R_c    Deaths    R_d     Cases    R_c    Deaths    R_d  ")
for dt in sorted(list(output)):
  print(dt,end="")
  for smooth in [0,1]:
    if smooth in output[dt]: print(output[dt][smooth],end="")
    else: print(" "*34,end="")
  print()
