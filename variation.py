# Exploring https://www.medrxiv.org/content/10.1101/2020.04.27.20081893v1

from scipy.special import gammainc
from scipy.stats import gamma as gammadist
import numpy as np

# Obtained (this version of) the number of initial infections by adding the number of new
# cases (as reported by Worldometers) on the days Feb 27, 28, 29, Mar 1, then multiplying
# by 10. Four days, because 1/gamma=4, and multiplying by 10 because p=0.1 (assumed
# probability of an infection being reported as a case).

situations=[
  {
    "Country": "Italy",
    "Population": 60360000,
    "initialinfections": 12310,
    "CV": 1,
    "SD": 0.00
  },
  {
    "Country": "Italy",
    "Population": 60360000,
    "initialinfections": 12310,
    "CV": 3,
    "SD": 0.00
  },
  {
    "Country": "Italy",
    "Population": 60360000,
    "initialinfections": 12310,
    "CV": 1,
    "SD": 0.68
  },
  {
    "Country": "Italy",
    "Population": 60360000,
    "initialinfections": 12310,
    "CV": 3,
    "SD": 0.61
  },
  {
    "Country": "Austria",
    "Population": 8859000,
    "initialinfections": 120,
    "CV": 1,
    "SD": 0.00
  },
  {
    "Country": "Austria",
    "Population": 8859000,
    "initialinfections": 120,
    "CV": 3,
    "SD": 0.00
  },
  {
    "Country": "Austria",
    "Population": 8859000,
    "initialinfections": 120,
    "CV": 1,
    "SD": 0.80
  },
  {
    "Country": "Austria",
    "Population": 8859000,
    "initialinfections": 120,
    "CV": 3,
    "SD": 0.77
  }
]

delta=1/4
gamma=1/4
rho=0.5
R0=2.7
p=0.1
days=487# Days from 2020-03-01 to 2021-07-01

# By experimentation, values of 10 for stepsperday and sbins give near to limiting
# behaviour (within small tolerances), so 100 for each is hopefully plenty.
stepsperday=100# Subdivision of each day
sbins=100# Number of bins for susceptibility (equally spaced by CDF)

# Caption to Figure 1 gives this time-dependence of the social distancing parameter:
# Return value, x, is effective social distancing number. Infection force is multiplied
# by 1-x.
def SD(sd0,day):
  if day<14: return day/14*sd0
  day-=14
  if day<31: return sd0
  day-=31
  if day<365: return (1-day/365)*sd0
  return 0

# Returns template distribution with mean 1 and coefficient of variation cv, using sbins bins
def getsusceptibilitydist(cv,sbins):
  
  # Shape parameter of Gamma distribution
  k=1/cv**2
  
  # Space out bins according to shape k+1 so that each bin represents an equal
  # (incomplete) expectation. Other bin choices are possible, but k+1 is better than using
  # k (which would make each bin represent an equal probability from the gamma
  # distribution) in the sense that you get limiting behaviour for a smaller value of
  # sbins. (This is to be expected because the infection force is the important quantity.)
  l=gammadist.ppf([i/sbins for i in range(sbins)],k+1)

  # Calculate:
  #   m0[i] = P[X<i/sbin]
  #   m1[i] = E[X; X<i/sbin]
  #   susc[i] = E[X | i/sbin < X < (i+1)/sbin], the representative susceptibility for bin i
  #   q[i] = P(i/sbin < X < (i+1)/sbin)
  m0=np.append(gammainc(k,l),1)
  m1=np.append(gammainc(k+1,l),1)
  susc=np.array([(m1[i+1]-m1[i])/(m0[i+1]-m0[i]) for i in range(sbins)])
  q=np.array([m0[i+1]-m0[i] for i in range(sbins)])

  return susc,q
  

seen=set()

for sit in situations:
  N=sit["Population"]
  cv=sit["CV"]
  sd0=sit["SD"]
  print("# Country:",sit["Country"])
  print("# Coefficient of Variation:",cv)
  print("# Max social distancing:",sd0)

  susc,q = getsusceptibilitydist(cv,sbins)
  S=q*N

  if cv not in seen:
    seen.add(cv)
    with open('q_CV%g'%cv,'w') as fp:
      for i in range(sbins):
        print("%9.3f   %9.7f"%(susc[i],q[i]),file=fp)
  
  E=np.zeros(sbins)
  I=np.zeros(sbins)
  beta=R0/(rho/delta+1/gamma)# NB there is a factor of 1/N error in formula (2) from the paper
  
  lam=0

  with open('output_%s_CV%g_SD%g'%(sit['Country'],cv,sd0),'w') as fp:
    for d0 in range(days*stepsperday):
      day=d0/stepsperday
  
      # Introduce initial infections by boosting the infection force for the expected duration of an infection (1/gamma days)
      if day<1/gamma: lam+=beta/N*sit['initialinfections']
      
      new=lam*susc*S*(1-SD(sd0,day))
      I+=(delta*E-gamma*I)/stepsperday
      E+=(new-delta*E)/stepsperday
      S+=-new/stepsperday
      Ssum=S.sum()
      Esum=E.sum()
      Isum=I.sum()
      lam=beta/N*(rho*Esum+Isum)
      print("%7.2f      %7.5f  %7.5f  %7.5f  %9.0f"%(day,Ssum/N,Esum/N,Isum/N,p*Isum),file=fp)
    print("# Proportion infected = %.1f%%"%((1-Ssum/N)*100))
  print("#")

